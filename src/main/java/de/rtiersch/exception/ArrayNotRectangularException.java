/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.exception;

import org.jetbrains.annotations.NotNull;

/**
 * The provided two dimensional array is not rectangualar.
 */
public class ArrayNotRectangularException
    extends IllegalArgumentException
{
    private static final long serialVersionUID = -5116101128118950844L;

    /**
     * Constructs an {@code IllegalArgumentException} with no
     * detail message.
     */
    public ArrayNotRectangularException()
    {
    }

    /**
     * Constructs a new {@code IllegalArgumentException}
     * class with an argument indicating the illegal index.
     *
     * @param p_index the illegal index.
     */
    public ArrayNotRectangularException(@NotNull final int... p_index)
    {
        super(ArrayNotRectangularException.buildString(p_index)
                  + "have a wrong amount of " + "elements.");
    }

    /**
     * Mimics the method already implemented in de.rtiersch.utils.
     * Is reimplemented to prevent circular imports. The Exception package
     * should be free of any imports (except NotNull and Nullable).
     * @param p_index indexes to concat together.
     * @return Concatenated ints.
     */
    @NotNull
    private static String buildString(@NotNull final int... p_index)
    {
        final StringBuilder l_sb = new StringBuilder();
        l_sb.append(p_index[0]);
        for (int l_i = 1; l_i < p_index.length; l_i++) {
            l_sb.append(", ");
            l_sb.append(p_index[l_i]);
        }
        return l_sb.toString();
    }

    /**
     * Constructs an {@code IllegalArgumentException} class
     * with the specified detail message.
     *
     * @param p_s the detail message.
     */
    public ArrayNotRectangularException(@NotNull final String p_s)
    {
        super(p_s);
    }

}
